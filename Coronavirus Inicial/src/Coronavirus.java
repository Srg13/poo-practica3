//Samantha Roldán García
//21 de Maig de 2020

//El Ramón m'ha donat l'idea de fer el RemoveSpikes per tal de que em funcionés la rotació
//El professor Marc m'ha ajudat a entendre com fer l'operació de Math.sinDegree() i Math.cosDegree()

import java.awt.*;
import acm.program.*;
import acm.graphics.*;

public class Coronavirus extends GraphicsProgram {
    int perimetre = 200; //si variem el perimetre s'imprimiràn més o menys punxes, amb 200 s'imprimeixen 24 punxes com a l'exemple
    public void run(){
        while(true){
            int rotate = -180;
            while(rotate<=180){
                exe(rotate);
                rotate++;
            }
        }
    }

//Execució de la rotació de les punxes
    public void exe(int rotate){
        circle();
        spikes(rotate,Color.GREEN);
        pause(40);
        removespikes(rotate);
    }

//Per dibuixar el cos del coronavirus
    public void circle(){
        int y=(getHeight()/2)-perimetre/2;
        int x=(getWidth()/2-perimetre/2);
        GOval corona = new GOval(x,y,perimetre,perimetre);
        corona.setFillColor(Color.GREEN);
        corona.setFilled(true);
        corona.setColor(Color.GREEN);
        add(corona);
        setBackground(Color.BLACK);
    }

//Per dibuixar les punxes al lloc on toca.
    public void spike(double firstx, double firsty, double angle, Color colo){
        GPolygon vertex = new GPolygon();
        vertex.addVertex(firstx-30*GMath.cosDegrees(angle),firsty-30*GMath.sinDegrees(angle)); //la punxa que quedarà dintre del cercle
        vertex.addVertex(firstx+20*GMath.cosDegrees(angle+25),firsty+20*GMath.sinDegrees(angle+25));//per dibuixar la part de sobre del cos del triangle
        vertex.addVertex(firstx+20*GMath.cosDegrees(angle-25),firsty+20*GMath.sinDegrees(angle-25));//per dibuixar la part inferior del cos del triangle
        vertex.setFilled(true);
        vertex.setFillColor(colo);
        vertex.setColor(colo);
        add(vertex);
    }

    public void spikes(int entrada, Color colo){
        double angle= 0+entrada;
        double firsty=(getHeight()/2);
        double firstx=(getWidth()/2);
        double x;
        double y;
        for(int i=0;i<25;i++){
            x=firstx+(perimetre/2)*GMath.cosDegrees(angle);
            y=firsty+(perimetre/2)*GMath.sinDegrees(angle);
            spike(x,y,angle,colo);
            angle=angle+15;
        }
    }

//Creo una funció que borri les punxes d'entrada per tal de que al fer la rotació funcioni el programa
    public void removespikes(int entrada){spikes(entrada, Color.BLACK);}
}