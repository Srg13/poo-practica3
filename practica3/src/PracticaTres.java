/**
 * Samantha Roldán García
 * 10/06/2020
 * Nuevo código para la práctica 3
 */

import java.awt.*;
import acm.program.*;
import acm.graphics.*;

public class PracticaTres extends GraphicsProgram {
    /**
     * Utilizo la clase recomendada en el pdf para crear una matriz que guardará la matriz de los puntos
     * que tiene cada punta del triángulo.
     */
    public GPoint[][]puntos = new GPoint[24][3];

    public void run() {
        centro();
        /**
         * Creo un bucle para ubicar las puntas por primera vez, para ello,
         * traslado el código que había en extremo y añado el setFillColor.
         * Me deshago de los auxiliares y los substituyo por valores.
         */
        for(int i=0; i < 24; i++){
            puntos[i] = new GPoint[]{new GPoint(getWidth()/2 + (80) * GMath.cosDegrees(i),  getHeight()/2 + (80) * GMath.sinDegrees(i)), new GPoint(getWidth()/2 + (120) * GMath.cosDegrees(i-4), getHeight()/2 + (120) * GMath.sinDegrees(i-4)), new GPoint(getWidth()/2 + (120) * GMath.cosDegrees(i+4), getHeight()/2 + (120) * GMath.sinDegrees(i+4))};
            GPolygon punta = new GPolygon(puntos[i]);
            punta.setFilled(true);
            punta.setFillColor(Color.GREEN);
            punta.setColor(Color.GREEN);
            add(punta);
        }
        double x=0;
        while(true){
            programa(x);
            x++;
        }
    }

    /**
     * Optimizo el código de forma que creo un 'double perimetro' de 200, así evito establecer constantes fijas.
     */
    public void centro() {
        double perimetro=200;
        double x = (getWidth()/2) - perimetro/2;
        double y = (getHeight()/2) - perimetro/2;
        GOval oval = new GOval(x, y, perimetro, perimetro);
        oval.setFilled(true);
        oval.setFillColor(Color.GREEN);
        oval.setColor(Color.GREEN);
        setBackground(Color.BLACK);
        add(oval);
    }

    public void programa(double x){
        pause(40);
        double radio = 100;
        /**
         * Creo un bucle para calcular los puntos de la punta a partir del radio del circulo
         */
        for (int i = 0, j = 0; j < puntos.length; i+=(360/24), j++){
            puntos[j] = new GPoint[]{new GPoint(getWidth()/2 + (radio-20) * GMath.cosDegrees(i+x),  getHeight()/2 + (radio-20) * GMath.sinDegrees(i+x)), new GPoint(getWidth()/2 + (radio+20) * GMath.cosDegrees(i-4+x), getHeight()/2 + (radio+20) * GMath.sinDegrees(i-4+x)), new GPoint(getWidth()/2 + (radio+20) * GMath.cosDegrees(i+4+x), getHeight()/2 + (radio+20) * GMath.sinDegrees(i+4+x))};
        }
        removeAll();
        centro();
        /**
         * Creo un segundo bucle para añadir los puntos en el circulo
         */
        for (int i = 0; i < 24; i++){
            GPolygon punta = new GPolygon(puntos[i]);
            punta.setFilled(true);
            punta.setFillColor(Color.GREEN);
            punta.setColor(Color.GREEN);
            add(punta);
        }
    }
}

